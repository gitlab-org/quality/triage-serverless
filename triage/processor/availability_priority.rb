# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  class AvailabilityPriority < Processor
    react_to 'issue.*', 'merge_request.*'

    def applicable?
      event.from_gitlab_org? &&
        event.resource_open? &&
        has_availability_label?
    end

    def process
      process_availability_priority
    end

    private

    def has_availability_label?
      event.label_names.include?('availability')
    end

    def process_availability_priority
      case severity
      when 1, 2
        ensure_priority(accept: [1])
      when 3
        ensure_priority(accept: [2, 1])
      when 4
        ensure_priority(accept: [3, 2, 1])
      end
    end

    def ensure_priority(accept:)
      default = accept.first

      if priority.nil?
        add_comment("/label ~priority::#{default}")
      elsif priority > default
        accept_list = accept.map { |p| "~priority::#{p}" }.join(', or ')

        add_comment <<~MARKDOWN.chomp
          ~severity::#{severity} with ~availability can only have #{accept_list}

          Please see the [Availability prioritization guidelines](https://about.gitlab.com/handbook/engineering/performance/#availability) for more detail.

          /unlabel ~priority::#{priority}
          /label ~priority::#{default}
        MARKDOWN
      end
    end

    def severity
      @severity ||= min_label('severity')
    end

    def priority
      @priority ||= min_label('priority')
    end

    def min_label(name)
      event.label_names.grep(/\A#{name}::\d\z/) do |label|
        label[-1].to_i
      end.min # In case there are multiple of them
    end
  end
end
