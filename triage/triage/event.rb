# frozen_string_literal: true

require 'gitlab'
require 'time'

require_relative 'error'

module Triage
  class Event
    ObjectKindNotProvidedError = Class.new(Triage::ClientError)

    # https://gitlab.com/gitlab-renovate-bot
    GITLAB_RENOVATE_BOT_ID = 8420142

    # https://gitlab.com/project_24673064_bot
    CONTAINER_SCANNING_BOT_ID = 8935721

    # Automation users that are not part of the GitLab groups for security reasons
    AUTOMATION_IDS = [
      GITLAB_RENOVATE_BOT_ID,
      CONTAINER_SCANNING_BOT_ID
    ].freeze
    PROJECT_SERVICE_ACCOUNT_REGEX = /^project_\d+_bot(\d+)?$/.freeze

    ISSUE = 'issue'
    MERGE_REQUEST = 'merge_request'
    NOTE = 'note'

    GITLAB_PROJECT_ID = 278964

    attr_reader :payload

    def initialize(payload)
      @payload = payload
    end

    def self.build(payload)
      payload['object_kind'].yield_self do |kind|
        if kind.nil?
          raise ObjectKindNotProvidedError, '`object_kind` not provided!'
        end

        case kind
        when ISSUE
          IssueEvent.new(payload)
        when MERGE_REQUEST
          MergeRequestEvent.new(payload)
        when NOTE
          NoteEvent.new(payload)
        else
          raise "Unknown object: #{kind.inspect}"
        end
      end
    end

    def key
      "#{object_kind}.#{action}"
    end

    def issue?
      object_kind == ISSUE
    end

    def merge_request?
      object_kind == MERGE_REQUEST
    end

    def note?
      object_kind == NOTE
    end

    def user
      payload.fetch('user', {})
    end

    def user_username
      user['username']
    end

    def author_id
      payload.dig('object_attributes', 'author_id')
    end

    def created_at
      created_at_timestamp = payload.dig('object_attributes', 'created_at')
      Time.parse(created_at_timestamp)
    end

    def url
      payload.dig('object_attributes', 'url')
    end

    def new_comment
      payload.dig('object_attributes', 'description').to_s
    end
    alias_method :description, :new_comment

    def action
      payload.dig('object_attributes', 'action')
    end

    def new_entity?
      action == 'open'
    end

    def added_label_names
      current_label_names - previous_label_names
    end

    def removed_label_names
      previous_label_names - current_label_names
    end

    def label_names
      labels.map { |label_data| label_data['title'] }
    end

    def noteable_path
      "/projects/#{project_id}/#{noteable_kind}/#{iid}"
    end

    def from_gitlab_org?
      event_from_group?(Triage::GITLAB_ORG_GROUP)
    end

    def from_gitlab_com?
      event_from_group?(Triage::GITLAB_COM_GROUP)
    end

    def from_gitlab_org_gitlab?
      with_project_id?(GITLAB_PROJECT_ID)
    end

    def automation_author?
      AUTOMATION_IDS.include?(author_id)
    end

    def user_is_project_service_account?
      user_username.match?(PROJECT_SERVICE_ACCOUNT_REGEX)
    end

    def gitlab_org_author?
      Triage.gitlab_org_group_member_ids.include?(author_id)
    end

    def wider_community_author?
      !automation_author? &&
        !user_is_project_service_account? &&
        (!Triage.gitlab_org_group_member_ids.include?(author_id) || Triage.gitlab_core_team_community_members_group_member_ids.include?(author_id))
    end

    def wider_gitlab_com_community_author?
      !automation_author? &&
        !user_is_project_service_account? &&
        (!Triage.gitlab_com_group_member_ids.include?(author_id) || Triage.gitlab_core_team_community_members_group_member_ids.include?(author_id))
    end

    def jihu_contributor?
      Triage.jihu_team_member_ids.include?(author_id)
    end

    def with_project_id?(expected_project_id)
      project_id == expected_project_id
    end

    def project_id
      payload.dig('project', 'id')
    end

    def project_web_url
      payload.dig('project', 'web_url')
    end

    def project_path_with_namespace
      payload.dig('project', 'path_with_namespace')
    end

    def title
      payload.dig('object_attributes', 'title')
    end

    def object_kind
      payload.fetch('object_kind', 'unknown')
    end

    def resource_open?
      state == 'opened'
    end

    def noteable_kind
      raise NotImplementedError
    end

    def iid
      raise NotImplementedError
    end

    private

    def state
      raise NotImplementedError
    end

    def changes
      payload.fetch('changes', {})
    end

    def labels
      payload.fetch('labels', [])
    end

    def previous_labels
      changes.dig('labels', 'previous') || []
    end

    def current_labels
      if new_entity?
        labels
      else
        changes.dig('labels', 'current') || []
      end
    end

    def previous_label_names
      previous_labels.map { |l| l['title'] }
    end

    def current_label_names
      current_labels.map { |l| l['title'] }
    end

    def event_from_group?(group)
      %r{\A#{group}/}.match?(payload.dig('project', 'path_with_namespace'))
    end
  end

  class IssuableEvent < Event
    def iid
      payload.dig('object_attributes', 'iid')
    end

    private

    def state
      payload.dig('object_attributes', 'state')
    end

    def noteable_kind
      raise NotImplementedError
    end
  end

  class IssueEvent < IssuableEvent
    def weight
      payload.dig('object_attributes', 'weight')
    end

    private

    def noteable_kind
      'issues'
    end
  end

  class MergeRequestEvent < IssuableEvent
    def wip?
      payload.dig('object_attributes', 'work_in_progress')
    end

    def merge_event?
      action == 'merge'
    end

    def approval_event?
      payload.dig('object_attributes', 'action') == 'approval'
    end

    def approved_event?
      payload.dig('object_attributes', 'action') == 'approved'
    end

    private

    def noteable_kind
      'merge_requests'
    end
  end

  class NoteEvent < Event
    def iid
      payload.dig(noteable_type, 'iid')
    end

    def key
      "#{noteable_type}.#{object_kind}"
    end

    def new_comment
      payload.dig('object_attributes', 'note').to_s
    end

    def noteable_author_id
      payload.dig(noteable_type, 'author_id')
    end

    def by_noteable_author?
      user['id'] == noteable_author_id
    end

    def note_on_issue?
      noteable_type == ISSUE
    end

    def note_on_merge_request?
      noteable_type == MERGE_REQUEST
    end

    def noteable_type
      to_snake_case(payload.dig('object_attributes', 'noteable_type'))
    end

    private

    def state
      payload.dig(noteable_type, 'state')
    end

    def noteable_kind
      "#{noteable_type}s"
    end

    def to_snake_case(string)
      string.gsub(/(?<=[a-z])[A-Z]/, '_\0').downcase
    end
  end
end
