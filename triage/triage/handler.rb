# frozen_string_literal: true

require_relative '../processor/availability_priority'
require_relative '../processor/backstage_label'
require_relative '../processor/customer_label'
require_relative '../processor/deprecated_label'
require_relative '../processor/doc_community_contribution'
require_relative '../processor/engineering_allocation_labels_reminder'
require_relative '../processor/hackathon_label'
require_relative '../processor/infradev_label_transition'
require_relative '../processor/jihu_contribution'
require_relative '../processor/label_inference'
require_relative '../processor/legal_disclaimer_on_direction_resources'
require_relative '../processor/merge_request_ci_title_label'
require_relative '../processor/merge_request_help'
require_relative '../processor/new_pipeline_on_approval'
require_relative '../processor/reactive_labeler'
require_relative '../processor/reactive_reviewer'
require_relative '../processor/thank_community_contribution'
require_relative '../processor/ux_community_contribution'
require_relative 'listener'

module Triage
  class Handler
    DEFAULT_PROCESSORS = [
      AvailabilityPriority,
      BackstageLabel,
      CustomerLabel,
      DeprecatedLabel,
      DocCommunityContribution,
      EngineeringAllocationLabelsReminder,
      HackathonLabel,
      InfradevLabelTransition,
      JiHuContribution,
      LabelInference,
      LegalDisclaimerOnDirectionResources,
      MergeRequestCiTitleLabel,
      MergeRequestHelp,
      NewPipelineOnApproval,
      ReactiveLabeler,
      ReactiveReviewer,
      ThankCommunityContribution,
      UxCommunityContribution
    ].freeze

    Result = Struct.new(:message, :error)

    def initialize(event, processors: DEFAULT_PROCESSORS)
      @event = event
      @processors = processors
    end

    def process
      results = Hash.new { |h, k| h[k] = Result.new }

      listeners[event.key].each do |processor|
        results[processor.name].message = processor.triage(event)
      rescue => e
        results[processor.name].error = e
      end

      results.select { |processor, result| result.message || result.error }
    end

    private

    attr_reader :event, :processors

    def listeners
      @listeners ||= processors.each_with_object(Hash.new { |h, k| h[k] = [] }) do |processor, result|
        processor.listeners.each do |listener|
          result[listener.event] << processor
        end
      end
    end
  end
end
