# frozen_string_literal: true

module Triage
  module Reaction
    def add_comment(body)
      path = "#{event.noteable_path}/notes"

      post_request(path, body)
    end

    def add_discussion(body)
      path = "#{event.noteable_path}/discussions"

      post_request(path, body)
    end

    private

    def post_request(path, body)
      if Triage.dry_run?
        puts "The following comment would have been posted to #{PRODUCTION_API_ENDPOINT}#{path}:"
        puts "```\n#{body}\n```"
      else
        Triage.api_client.post(path, body: { body: body })
      end

      body
    end
  end
end
