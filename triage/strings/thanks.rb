# frozen_string_literal: true

module Triage
  module Strings
    module Thanks
      timely_manner = <<~MARKDOWN.chomp
        * Our [Merge Request Coaches](https://about.gitlab.com/company/team/?department=merge-request-coach)
        will ensure your contribution is reviewed in a timely manner[*](https://about.gitlab.com/handbook/engineering/quality/merge-request-triage).
      MARKDOWN
      danger_setup = "* If you haven't, please set up a [`DANGER_GITLAB_API_TOKEN`](https://docs.gitlab.com/ee/development/dangerbot.html#limitations)."
      group_label = '* You can comment `@gitlab-bot label ~"group::"` to add a [group label](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#group-labels).'
      follow_up = '* After a few days, feel free to ask `@gitlab-bot help` or [ping a Merge Request Coach](https://about.gitlab.com/company/team/?department=merge-request-coach).'
      read_more = '* Read more on [how to get help](https://about.gitlab.com/community/contribute/#getting-help).'

      runner_body = <<~MARKDOWN.chomp
        Some contributions require several iterations of review and we try to mentor contributors
        during this process. However, we understand that some reviews can be very time consuming.
        If you would prefer for us to continue the work you've submitted now or at any point in the
        future please let us know.

        If you're okay with being part of our review process (and we hope you are!), there are
        several initial checks we ask you to make:

        * The merge request description clearly explains:
          * The problem being solved.
          * The best way a reviewer can test your changes (is it possible to provide an example?).
        * If the pipeline failed, do you need help identifying what failed?
        * Check that Go code follows our [Go guidelines](https://docs.gitlab.com/ee/development/go_guide/index.html#code-review).
        * Read our [contributing to GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/main/CONTRIBUTING.md#contribute-to-gitlab-runner)
        document.
      MARKDOWN

      www_gitlab_com_body = <<~MARKDOWN.chomp
        I'll notify the Website team about your Merge Request and they will get back to you as soon
        as they can.
        If you don't hear from someone in a reasonable amount of time, please ping us again in a
        comment and mention @gl-website.
      MARKDOWN

      intro_thanks = <<~MARKDOWN.chomp
        :wave: @%<author_username>s

        Thank you for your contribution to GitLab. We believe that [everyone can contribute](https://about.gitlab.com/company/mission/#mission)
        and contributions like yours are what make GitLab great!
      MARKDOWN

      signoff_thanks = <<~MARKDOWN.chomp
        *This message was [generated automatically](https://about.gitlab.com/handbook/engineering/quality/triage-operations/#ensure-quick-feedback-for-community-contributions).
        You're welcome to [improve it](https://gitlab.com/gitlab-org/quality/triage-serverless/-/blob/master/triage/strings/thanks.rb).*

        /label ~"Community contribution"
      MARKDOWN

      DEFAULT_THANKS = <<~MARKDOWN.chomp
        #{intro_thanks}

        #{timely_manner}
        #{group_label}
        #{follow_up}
        #{read_more}
      
        #{signoff_thanks}
      MARKDOWN

      GITLAB_THANKS = <<~MARKDOWN.chomp
        #{intro_thanks}

        #{timely_manner}
        #{danger_setup}
        #{group_label}
        #{follow_up}
        #{read_more}
      
        #{signoff_thanks}
      MARKDOWN

      RUNNER_THANKS = <<~MARKDOWN.chomp
        #{intro_thanks}

        #{runner_body}

        #{signoff_thanks}
      MARKDOWN

      WWW_GITLAB_COM_THANKS = <<~MARKDOWN.chomp
        #{intro_thanks}

        #{www_gitlab_com_body}

        #{signoff_thanks}
      MARKDOWN
    end
  end
end
