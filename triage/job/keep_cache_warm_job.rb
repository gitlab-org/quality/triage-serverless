# frozen_string_literal: true

require_relative '../triage'
require 'sucker_punch'

module Triage
  class KeepCacheWarmJob
    include SuckerPunch::Job
    max_jobs 1

    def perform
      puts "Keeping the cache warm..."

      Triage::GROUP_CACHE.each do |group, cache_definitions|
        cache_definitions.each do |definition|
          Triage.public_send(definition.fetch(:name), fresh: true)
        end
      end

      self.class.perform_in(Triage::GROUP_CACHE_DEFAULT_EXPIRATION - 60)
    end
  end
end
