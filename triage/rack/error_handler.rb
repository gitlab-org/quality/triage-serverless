require 'rack'

require_relative '../triage/sentry'

module Triage
  module Rack
    class ErrorHandler < Struct.new(:app)
      def call(env)
        app.call(env)
      rescue => error
        Raven.capture_exception(error)
        ::Rack::Response.new([JSON.dump(status: :error, error: error.class, message: error.message)], 500).finish
      end
    end
  end
end
