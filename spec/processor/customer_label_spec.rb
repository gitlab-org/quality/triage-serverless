# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/customer_label'
require_relative '../../triage/triage/event'

RSpec.describe Triage::CustomerLabel do
  include_context 'with event' do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'merge',
        new_comment: new_comment,
        from_gitlab_org?: true
      }
    end
  end

  let(:event_author) { Triage::Event::ENG_PROD_TEAM_USERNAMES.first }
  let(:contribution_type) { 'type::bug' }
  let(:url) { 'http://gitlab.com/mr_url' }
  let(:org_name) { 'org' }

  subject { described_class.new(event) }

  include_examples 'registers listeners', ["issue.close", "issue.note", "issue.open", "issue.reopen", "issue.update", "merge_request.approval", "merge_request.approved", "merge_request.close", "merge_request.merge", "merge_request.note", "merge_request.update", "merge_request.open", "merge_request.reopen", "merge_request.unapproval", "merge_request.unapproved"]

  describe '#applicable?' do
    context 'when new comment has gitlab.zendesk.com' do
      let(:new_comment) { 'gitlab.zendesk.com' }

      include_examples 'event is applicable'
    end

    context 'when new comment has gitlab.my.salesforce.com' do
      let(:new_comment) { 'gitlab.my.salesforce.com' }

      include_examples 'event is applicable'
    end

    context 'when there is a customer link' do
      let(:event) do
        Triage::Event.build(
          JSON.parse(File.read("#{__dir__}/../fixture/note_on_issue.json"))
        )
      end

      before do
        allow(subject).to receive(:has_customer_link?).and_return(true)
      end

      include_examples 'event is applicable'
    end

    context 'when new comment has gitlab.com' do
      let(:new_comment) { 'gitlab.com' }

      include_examples 'event is not applicable'
    end

    context 'when new comment is empty' do
      let(:new_comment) { '' }

      include_examples 'event is not applicable'
    end

    context 'when new comment project is not within `gitlab-org`' do
      let(:new_comment) { 'gitlab.zendesk.com' }

      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    context 'when new comment has gitlab.zendesk.com' do
      let(:new_comment) { 'gitlab.zendesk.com' }

      it 'calls add_customer_label' do
        expect(subject).to receive(:add_customer_label)

        subject.process
      end
    end

    context 'when new comment has gitlab.my.salesforce.com' do
      let(:new_comment) { 'gitlab.my.salesforce.com' }

      it 'calls add_customer_label' do
        expect(subject).to receive(:add_customer_label)

        subject.process
      end
    end

    context 'when there is a customer link' do
      let(:event) do
        Triage::Event.build(
          JSON.parse(File.read("#{__dir__}/../fixture/note_on_issue.json"))
        )
      end

      before do
        allow(subject).to receive(:has_customer_link?).and_return(true)
      end

      it 'uses noteable_path to post a comment to add a customer label' do
        expect_comment_request(event: event, body: '/label ~customer') do
          subject.process
        end
      end
    end
  end
end
