# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/ux_community_contribution'
require_relative '../../triage/triage/event'

RSpec.describe Triage::UxCommunityContribution do
  include_context 'slack posting context'
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'open',
        from_gitlab_org?: true,
        wider_community_author?: true,
        issue?: false,
        merge_request?: true,
        project_id: project_id,
        iid: merge_request_iid,
        wip?: false,
        label_names: label_names,
        noteable_path: '/foo',
        project_web_url: 'https://gitlab.example/group/project',
        url: url,
        title: title
      }
    end
    let(:project_id) { 123 }
    let(:merge_request_iid) { 300 }
    let(:url) { 'http://gitlab.com/mr_url' }
    let(:title) { 'Merge request title' }
    let(:label_names) { [described_class::UX_LABEL] }
  end

  subject { described_class.new(event, messenger: messenger_stub) }

  before do
    stub_api_request(
      path: "/projects/#{event.project_id}/#{event.object_kind}s/#{event.iid}/notes",
      query: { per_page: 100 },
      response_body: [])
  end

  include_examples 'registers listeners', ["merge_request.update", "merge_request.open"]

  it_behaves_like 'processor slack options', '#ux-community-contributions'

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is not from a wider community author' do
      before do
        allow(event).to receive(:wider_community_author?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when resource is not opened' do
      before do
        allow(event).to receive(:resource_open?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is a Draft' do
      before do
        allow(event).to receive(:wip?).and_return(true)
      end

      include_examples 'event is not applicable'
    end

    context 'when other label is added' do
      let(:added_label_names) { ['bug'] }

      include_examples 'event is not applicable'
    end

    context 'when no label is added' do
      include_examples 'event is not applicable'
    end

    context 'when UX label is added' do
      let(:added_label_names) { ['UX'] }

      include_examples 'event is applicable'
    end
  end

  describe '#process' do
    before do
      allow(messenger_stub).to receive(:ping)
    end

    shared_examples 'process UX merge request' do
      it 'posts a comment' do
        body = <<~MARKDOWN.chomp
          #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
          Thanks for helping us improve the UX of GitLab, your contribution is appreciated! We have pinged UX team members, stay tuned for their feedback.
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    it_behaves_like 'process UX merge request'
    it_behaves_like 'slack message posting' do
      before do
        allow(subject).to receive(:post_ux_comment)
      end

      let(:message_body) do
        <<~MARKDOWN
          Hi UX team, a new community contribution (#{title}) requires a UX review: #{url}.
        MARKDOWN
      end
    end
  end
end
