RSpec.shared_context 'with event' do |event_class = 'Triage::Event'|
  let(:event_attrs) { {} }
  let(:label_names) { [] }
  let(:added_label_names) { [] }
  let(:event) do
    instance_double(event_class, {
      object_kind: 'issue',
      action: 'open',
      user: { 'id' => 1, 'username' => 'root' },
      author_id: 42,
      key: 'issue.open',
      noteable_path: '/foo',
      label_names: label_names,
      added_label_names: added_label_names,
      resource_open?: true,
      payload: {}
    }.merge(event_attrs))
  end
end
