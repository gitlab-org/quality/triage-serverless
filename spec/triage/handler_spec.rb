# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/handler'

RSpec.describe Triage::Handler do
  include_context 'with event' do
    let(:event_attrs) do
      {
        note?: false
      }
    end
  end

  let(:processor1) do
    Class.new(Triage::Processor) do
      react_to 'issue.*'

      def self.name
        'processor1'
      end

      def process
        true
      end
    end
  end
  let(:processor2) do
    Class.new(Triage::Processor) do
      react_to 'merge_request.note'

      def self.name
        'processor2'
      end

      def process
        nil
      end
    end
  end
  let(:processor3) do
    Class.new(Triage::Processor) do
      react_to '*.*'

      def self.name
        'processor3'
      end

      def process
        true
      end
    end
  end

  subject { described_class.new(event, processors: [processor1, processor2, processor3]) }

  describe 'DEFAULT_PROCESSORS' do
    it 'includes all processor implementations' do
      expected = [
        Triage::AvailabilityPriority,
        Triage::BackstageLabel,
        Triage::CustomerLabel,
        Triage::DeprecatedLabel,
        Triage::DocCommunityContribution,
        Triage::EngineeringAllocationLabelsReminder,
        Triage::HackathonLabel,
        Triage::InfradevLabelTransition,
        Triage::JiHuContribution,
        Triage::LabelInference,
        Triage::LegalDisclaimerOnDirectionResources,
        Triage::MergeRequestCiTitleLabel,
        Triage::MergeRequestHelp,
        Triage::NewPipelineOnApproval,
        Triage::ReactiveLabeler,
        Triage::ReactiveReviewer,
        Triage::ThankCommunityContribution,
        Triage::UxCommunityContribution
      ]

      expect(described_class::DEFAULT_PROCESSORS).to eq(expected)
    end

    described_class::DEFAULT_PROCESSORS.each do |processor|
      it "reacts to something for #{processor}" do
        expect(processor.listeners).to be_any
      end
    end
  end

  describe '#process' do
    it 'executes processors that listen to the event' do
      expect(processor1).to receive(:triage).once.and_call_original
      expect(processor2).not_to receive(:triage)
      expect(processor3).to receive(:triage).once.and_call_original

      results = subject.process

      expect(results[processor1.name].message).to eq(true)
      expect(results[processor2.name]).to be_nil
      expect(results[processor3.name].message).to eq(true)
    end

    context 'when a processor raises an error' do
      let(:error) { RuntimeError.new }

      before do
        allow(processor1).to receive(:triage).and_raise(error)
      end

      it 'captures error' do
        results = subject.process

        expect(results[processor1.name].error).to eq(error)
        expect(results[processor2.name]).to be_nil
        expect(results[processor3.name].error).to be_nil
      end

      it 'executes subsequent processor' do
        expect(processor2).not_to receive(:triage)
        expect(processor3).to receive(:triage).once

        subject.process
      end
    end
  end
end
