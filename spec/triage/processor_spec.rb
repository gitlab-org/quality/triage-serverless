# frozen_string_literal: true
require 'spec_helper'

require_relative '../../triage/triage/processor'

describe Triage::Processor do
  describe '.react_to and .listeners' do
    context "react to 'issue.open', 'issue.note', 'merge_request.*'" do
      include_examples 'registers listeners', ['issue.note', 'issue.open', 'merge_request.approval', 'merge_request.approved', 'merge_request.close', 'merge_request.merge', 'merge_request.note', 'merge_request.update', 'merge_request.open', 'merge_request.reopen', 'merge_request.unapproval', 'merge_request.unapproved'] do
        let(:processor) do
          Class.new(described_class) do
            react_to 'issue.open', 'issue.note', 'merge_request.*'
          end
        end
      end
    end

    context "react to '*.*'" do
      include_examples 'registers listeners', ['issue.note', 'issue.open', 'issue.close', 'issue.reopen', 'issue.update', 'merge_request.approval', 'merge_request.approved', 'merge_request.close', 'merge_request.merge', 'merge_request.note', 'merge_request.update', 'merge_request.open', 'merge_request.reopen', 'merge_request.unapproval', 'merge_request.unapproved'] do
        let(:processor) do
          Class.new(described_class) do
            react_to '*.*'
          end
        end
      end
    end
  end

  describe '#triage' do
    subject { described_class.new(double(:event)) }

    context 'if event is applicable' do
      it 'calls hook methods' do
        expect(subject).to receive(:applicable?).ordered.and_return(true)
        expect(subject).to receive(:before_process).ordered
        expect(subject).to receive(:process).ordered
        expect(subject).to receive(:after_process).ordered

        subject.triage
      end

      context 'with a subclass' do
        let(:processor) do
          Class.new(described_class) do
            def process
              'a message'
            end
          end
        end

        subject { processor.new(double(:event)) }

        it 'returns the result of #process' do
          expect(subject.triage).to eq('a message')
        end
      end
    end

    context 'if event is not applicable' do
      it 'does not call hooks' do
        expect(subject).to receive(:applicable?).and_return(false)
        expect(subject).not_to receive(:before_process)
        expect(subject).not_to receive(:process)
        expect(subject).not_to receive(:after_process)

        subject.triage
      end
    end
  end
end
